# import the necessary packages
import math

import imutils
import numpy as np
import cv2
from imutils.video import VideoStream
from skimage.measure import compare_ssim

from tuning import Tuning

tunes = Tuning()
pts = tunes.pts
vs = VideoStream(src=0).start()
namedWindow = cv2.namedWindow(tunes.windowName)
cv2.createTrackbar('R', tunes.windowName, tunes.radius, 255, lambda x: tunes.update_radius(x))
cv2.createTrackbar('V', tunes.windowName, tunes.min_val, 1000, lambda x: tunes.update_min_val(x))
cv2.createTrackbar('B', tunes.windowName, tunes.buffer, 500, lambda x: tunes.update_buffer(x))
cv2.createTrackbar('T', tunes.windowName, tunes.thickness, 30, lambda x: tunes.update_thickness(x))


def create_path(points, frame_shape):
    image = np.zeros(frame_shape, np.uint8)
    for i, point in enumerate(points):
        if i != len(points) - 1:
            cv2.line(image, point, points[i + 1], (0, 0, 255), tunes.thickness)
    return image


def deg_to_compass(num):
    val = int((num / 22.5) + .5)
    arr = ["N", "NNE", "NE", "ENE", "E", "ESE", "SE", "SSE", "S", "SSW", "SW", "WSW", "W", "WNW", "NW", "NNW"]
    direction = arr[(val % 16)]
    print(num, direction)
    return direction


def get_point_dir(c, n):
    return deg_to_compass(math.degrees(math.atan2(n[1] - c[1], n[0] - c[0])))


def distance(c, n):
    x = abs(n[0] - c[0])
    y = abs(n[1] - c[1])
    return math.hypot(x, y)


def analyze_path(points):
    cur_point = points[0]
    print(cur_point, points[-1], get_point_dir(cur_point, points[-1]))
    cur_dir = ""
    total_dir = ""
    for i in range(1, len(points)):
        next_point = points[i]
        next_dir = get_point_dir(cur_point, next_point)
        cur_point = next_point
        if next_dir != cur_dir:
            cur_dir = next_dir
            total_dir += f"{next_dir}  "
    return total_dir[:-1]


def main():
    current_img = 0
    wait_frames = 0
    while cv2.getWindowProperty(tunes.windowName, 0) >= 0:
        # grab the current frame
        frame = vs.read()
        if frame is None:
            break
        frame = cv2.flip(imutils.resize(frame, width=600, height=450), 1)
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        gray = cv2.GaussianBlur(gray, (41, 41), 0)
        (minVal, maxVal, minLoc, maxLoc) = cv2.minMaxLoc(gray)
        if maxVal > tunes.min_val:
            if len(pts) == 0 or maxLoc != pts[0]:
                pts.appendleft(maxLoc)
        else:
            wait_frames += 1
        # display the results of our newly improved method
        key = cv2.waitKey(1) & 0xFF
        path = create_path(pts, frame.shape)
        stacked_img = cv2.add(frame, path)
        cv2.imshow(tunes.windowName, stacked_img)
        # if the 'q' key is pressed, stop the loop
        if key == ord("q"):
            break
        if key == ord("r"):
            if len(pts) > 0:
                path_analysis = analyze_path(pts)
                print(path_analysis)
                cv2.imwrite(f"test{current_img}.jpg", path)
                current_img += 1
        if key == ord("c"):
            pts.clear()
    vs.stop()
    # close all windows
    cv2.destroyAllWindows()


main()
