import cv2
from skimage.measure import compare_ssim

currentImg = 1


img1 = cv2.cvtColor(cv2.imread(f"incindia.jpg"), cv2.COLOR_BGR2GRAY)
img2 = cv2.cvtColor(cv2.imread(f"test3.jpg"), cv2.COLOR_BGR2GRAY)
(score, diff) = compare_ssim(img1, img2, full=True)
diff = (diff * 255).astype("uint8")
print(f"Incindia vs Test:  Score: {score}")
currentImg += 1
