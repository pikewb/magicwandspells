from collections import deque
from dataclasses import dataclass


@dataclass
class Tuning(object):
    radius: int = 40
    pts: deque = deque(maxlen=200)
    min_val: int = 250
    buffer: int = 200
    windowName: str = "Magic Wand"
    thickness: int = 10

    def update_radius(self, rads):
        self.radius = rads

    def update_buffer(self, buff):
        if buff > 0:
            self.buffer = buff

    def update_min_val(self, val):
        self.min_val = val

    def add_pt(self, pt):
        self.pts.appendleft(pt)
        while len(self.pts) > self.buffer:
            self.pts.pop()

    def update_thickness(self, thick):
        self.thickness = thick
